print("String Functions and Multiple Declaration Sample Program")
name = input('Enter your Name : ')
print(type(name))
print(name.upper())
print(name.lower())
print(name.replace("ou", "ountharia"))
print('Multiple Declaration')
x,y,z = 'a', 10, True

print(x,y,z)
