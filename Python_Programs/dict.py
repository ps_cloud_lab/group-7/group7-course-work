fruits = {"apple":3,"banana":5,"kiwi":2}
print(fruits)
fruits["apple"]='4'
print(fruits)
fruits["mango"]=10
print(fruits)
fruits["favorite"]=["apple","mango"]
print(fruits)
fruits["favorite"].append("banana")
print(fruits)
print(fruits["favorite"])
fruits["favorite"].append("green apple")
print(fruits["favorite"])
print("Fruits:",fruits)
fruits.update(apple=25,strawberry=2,favorite=['caramel apple'])
print("fruits after update",fruits)

def groups_per_user(group_dictionary):
	user_groups = {}
	# Go through group_dictionary
	print(group_dictionary)
	for group, users in group_dictionary.items():
		# Now go through the users in the group
		for user in users:
			if user in user_groups:
				#print(user,"in",user_groups,group)
				user_groups[user].append(group)
			else:
				user_groups[user]=[group]
			#print(user,group)
			# Now add the group to the the list of
# groups for this user, creating the entry
# in the dictionary if necessary

	return(user_groups)

print(groups_per_user({"local": ["admin", "userA"],
		"public":  ["admin", "userB"],
		"administrator": ["admin"] }))
