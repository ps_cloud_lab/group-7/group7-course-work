##1. Operational Excellence
- Perform operations as code  
- Make frequent, small, reversible changes  
- Refine operations procedures frequently  
- Anticipate failure  
- Learn from all operational failures  

##2. Security
- There are seven design principles for security in the cloud:  
- Implement a strong identity foundation  
- Enable traceability  
- Apply security at all layers  
- Automate security best practices  
- Protect data in transit and at rest  
- Keep people away from data  
- Prepare for security events  
##3. Reliability
- There are five design principles for reliability in the cloud:  
- Automatically recover from failure  
- Test recovery procedures  
- Scale horizontally to increase aggregate workload availability  
- Stop guessing capacity  
- Manage change in automation  
##4. Performance Efficiency  
- There are five design principles for performance efficiency in the cloud:  
- Democratize advanced technologies  
- Go global in minutes  
- Use serverless architectures  
- Experiment more often  
- Consider mechanical sympathy  
##5. Cost Optimization  
- There are five design principles for cost optimization in the cloud:  
- Implement cloud financial management  
- Adopt a consumption model  
- Measure overall efficiency  
- Stop spending money on undifferentiated heavy lifting  
- Analyze and attribute expenditure  

##Trusted Advisor  

. 7 Core Checks  
- S3 bucket Permissions(notifies if public)  
- IAM use  
- MFA for Root account  
- Security Group - if specific ports are unrestricted)  
- EBS public snapshot  
- RDS public snapshot  
- Service quota  

There are 4 Support plans

1. Basic
2. Developer
3. Business
4. Enterprise
