# Enter your code here. Read input from STDIN. Print output to STDOUT

def getints():
    """Get a line of ints from STDIN and return a list of ints.
    """
    return list(map(int, input().split()))

def solve(ray, A, B):
    # A and B are disjoint sets of equal size.
    # likes = A
    # dislikes = B
    happiness = 0
    
    # Now what?
    # What if `ray = [5]*10_000`, and `A[-1] == 5`?
    #   How long would that take?
    #   lg(10_000) == 10_000 .bit_length() - 1 == 13
    #   - It takes 13 steps instead of 10,000 steps to search through a sorted array.
    
    # Create a lookup table.
    happinesses = {}
    for x in A:
        happinesses[x] = 1
    for x in B:
        happinesses[x] = -1
    
    # Now we're doing the lookups.
    for x in ray:
        # try:
        #     happiness += happinesses[x]
        # except KeyError:
        #     pass
        happiness += happinesses.get(x, 0)
    
    return happiness

# Cleaned up:
def solve(ray, A, B):
    # Create a lookup table.
    happinesses = {}
    for x in A:
        happinesses[x] = 1
    for x in B:
        happinesses[x] = -1
    
    # Add it up using lookups.
    happiness = 0
    for x in ray:
        happiness += happinesses.get(x, 0)
    
    return happiness

# Using `sum`:
def solve(ray, A, B) -> int:
    # Create a lookup table.
    happinesses = {}
    for x in A:
        happinesses[x] = 1
    for x in B:
        happinesses[x] = -1
    
    # Add it up using lookups.
    happiness = sum(happinesses.get(x, 0) for x in ray)
    
    return happiness

# Using `set`:
def solve(ray, A, B) -> int:
    likes = set(A)
    dislikes = set(B)

    # Add it up using lookups.
    happiness = 0
    for x in ray:
        if x in likes:
            happiness += 1
        elif x in dislikes:
            happiness -= 1
    
    return happiness


# Make it worse, by using list lookups:
def solve(ray, A, B) -> int:
    likes : list = A
    dislikes : list = B

    # Add it up using lookups.
    happiness = 0
    for x in ray:
        if x in likes:
            happiness += 1
        elif x in dislikes:
            happiness -= 1
    
    return happiness



# v = input()
# n, m = v.split(' ')
# n, m = int(n), int(m)
# n, m = map(int, v.split(' '))




# arr = list(map(int, input().split()))
# arr = input().split()
# arr == ['1', '5', '3']

# for x in arr:
#     ...

# for i in range(len(arr)):
#     x = arr[i]
    # ...

# for i, x in enumerate(arr):
#     # i,x = (0, '1')
#     # i,x = (1, '5')
#     # i,x = (2, '3')
#     arr[i] = int(x)

# b = arr
# arr = [int(x) for x in arr]
# arr = list(map(int, arr))

# arr = [int(x) for x in input().split()]
# arr1 = []
# for x in arr:
#     arr1.append(int(x))
# arr = arr1
# del arr1

# a = list(map(int, input().split()))
# b = list(map(int, input().split()))

# Get input.
n, m = getints()
arr = getints()
a = getints()
b = getints()

happiness = solve(arr, a, b)

print(happiness)



