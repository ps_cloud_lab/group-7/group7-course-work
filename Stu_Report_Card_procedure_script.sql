create or replace procedure Student_Result_Card( Stu_id int)
LANGUAGE plpgsql
as $$
declare
	i record;
	id int;
	name varchar(20);
	class_grade int;
	total real = 0;
	avg_mark real =0;
	grd varchar(3);
begin

	raise notice '%Report Card',lpad(' ',25, ' '); 
	raise notice '%', rpad('--',65,'-');

	select student_id, student_name, Student_class
	  into id, name, class_grade 
	  from student 
	 where student_id = stu_id;

	raise notice 'Student_ID : % 	Name: %	 Class : % grade', id, name, class_grade; 
	raise notice '%', rpad('--',65,'-');

	/** fetch marks of the student id given in the input**/
	for i in (select b.subject_id, c.subject_name, round(marks::numeric,1) marks
			 from student_marks b, subject_master c 
			where b.subject_id = c.subject_id
			  and b.student_id = coalesce(stu_id, b.student_id)
			) 
	loop
		total = i.marks + total;
		raise notice '%Subject : % Mark: %', lpad(' ',8,' '),rpad(i.subject_name,20,' '), i.marks; 
	end loop;

	/** Calculting the Grade **/
	avg_mark = total/5;
	if (avg_mark >= 85) and (avg_mark <-100) then
		grd = 'A+';
	elsif (avg_mark >=70) and (avg_mark <85) then
		grd = 'B';
	elsif (avg_mark >=55) and (avg_mark <70) then
		grd = 'C';
	elsif (avg_mark >=40) and (avg_mark <55) then
		grd = 'D';
	elsif (avg_mark < 40 ) then
		grd = 'F';
	end if;
	
	
	
	raise notice '%', rpad('--',65,'-');
	raise notice '%Total : % %    Grade : %', lpad(' ',10, ' '),rpad(' ',25,' '), total, grd;--, grade(total); 
end;
$$
