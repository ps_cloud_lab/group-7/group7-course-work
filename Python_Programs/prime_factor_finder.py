def print_prime_factors(number):
  # Start with two, which is the first prime
  factor = 2
  print(f"Prime Factor of {number} is : ", end=" ")
  # Keep going until the factor is larger than the number 
  while factor <= number:
    # Check if factor is a divisor of number
    if number % factor == 0:
      # If it is, print it and divide the original number
      print(factor, end="  ")
      number = number / factor
    else:
      # If it's not, increment the factor by one
      factor += 1
  return "Done"

def Menu():
    print("\n\n\n"+"*"*40+"   Menu   "+"*"*40+"\n")
    print(" "*40+"1. Find Prime Number")
    print(" "*40+"2. Exit \n")
    try:
        choice_number = int(input(" "*25+"Enter your Choice(1 or 2): "))
        if choice_number!=1:
            return False;
        return True
    except:
        return False
              
while Menu():
    print("\n"+"*"*90+"\n"+" "*40+"FINDING PRIME NUMBER\n"+"*"*90+"\n")
    n=int(input("Enter a Number to find its prime factors : "))
    print_prime_factors(n)

# Should print 2,2,5,5
# DO NOT DELETE THIS COMMENT

