# Sample commands

Create an S3 bucket
```
aws s3api create-bucket --bucket {bucket name} --region {region name} --create-bucket-configuration LocationConstraint={region name}
```

Create IAM user and roles
- Create a user

```
aws iam create-user --user-name {user name}
```

- Attach profile to the newly created user

```
aws iam create-login-profile --user-name {username} --password {password} 
```