# Debugging instructions
---

Lab 17 on Python is debugging the following code to find what is wrong and fix it so it works correctly

```
# Module Lab: Caesar Cipher Program Bug #1
#
# In a previous lab, you created a Caesar cipher program. This version of
# the program is buggy. Use a debugger to find the bug and fix it.

# Double the given alphabet
def getDoubleAlphabet(alphabet):
    doubleAlphabet = alphabet + alphabet
    return doubleAlphabet

# Get a message to encrypt
def getMessage():
    stringToEncrypt = input("Please enter a message to encrypt: ")
    return stringToEncrypt

# Get a cipher key
def getCipherKey():
    shiftAmount = input("Please enter a key (whole number from 1-25): ")
    return shiftAmount

# Encrypt message
def encryptMessage(message, cipherKey, alphabet):
    encryptedMessage = ""
    uppercaseMessage = ""
    uppercaseMessage = message.upper()
    for currentCharacter in uppercaseMessage:
        position = alphabet.find(currentCharacter)
        newPosition = position + cipherKey
        if currentCharacter in alphabet:
            encryptedMessage = encryptedMessage + alphabet[newPosition]
        else:
            encryptedMessage = encryptedMessage + currentCharacter
    return encryptedMessage

# Decrypt message
def decryptMessage(message, cipherKey, alphabet):
    decryptKey = -1 * int(cipherKey)
    return encryptMessage(message, decryptKey, alphabet)

# Main program logic
def runCaesarCipherProgram():
    myAlphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    print(f'Alphabet: {myAlphabet}')
    myAlphabet2 = getDoubleAlphabet(myAlphabet)
    print(f'Alphabet2: {myAlphabet2}')
    myMessage = getMessage()
    print(myMessage)
    myCipherKey = getCipherKey()
    print(myCipherKey)
    myEncryptedMessage = encryptMessage(myMessage, myCipherKey, myAlphabet2)
    print(f'Encrypted Message: {myEncryptedMessage}')
    myDecryptedMessage = decryptMessage(myEncryptedMessage, myCipherKey, myAlphabet2)
    print(f'Decrypted Message: {myDecryptedMessage}')

# Main logic
runCaesarCipherProgram()
```

# Starting debugging
 There is something wrong and every programmer has an idea where it might have gone wrong. Either because the execution errored out showing the line number or as the programmer knows where what is going on. In order to start debugging, we need to apply a break point on where we want the execution to stop and control should be given to the programmer to see what is going on, in the code.

![Break Point](./Images/Python/Breakpoint.png)

We have started the execution at the start of the program so we can move through the code. We are not sure where the error lies.

Now we need to start debugging session. If we run the code, it will not stop at this location, so we need to click on debug rather that run the code

![Start Debugging](./Images/Python/Start_Debugging.png)

In the screen shot above, we need to select Debug python file from the menu on the right and once we click it, debug menu will appear and code execution will pause at the selected break point.

At this time, the execution is paused and waiting for the programmer to proceed as it pleases. On the left of the screen we have four windows. 
- **Variables:** This window shows all local and global variables in this code and their current value
- **Watch:** This is a window where we can add variables which we want to watch every step of the way how their values change
- **Call Stack:** All variables will be in reference to the stack frame
- **Breakpoints:** This is a list of all break points in case we need to skip a break point for this execution

Lets look at the debugging navigation menu on top how it looks. 

![Debugging Navigation Menu](./Images/Python/DebugNavigations.png)

We are not discussing configuration in this help file, but we can specify some settings before entering into debugging. 

![Debugging Configuration](./Images/Python/Branches.png)

Menu options are
- **Continue / Pause F5 :** Resume the execution of the program from this point
- **Step Over F10 :** Execute the function without getting into the function
- **Step Into F11 :** Continue debugging into the function, we need to see what is going on in this function
- **Step Out ⇧F11 :** We have debugged enough in this function, continue executing till we are out of the function
- **Restart ⇧⌘F5 :** Restart debugging from start
- **Stop ⇧F5 :** Stop debugging

Once we hit an error, it will be shown on the line where the error occurs

![Error](./Images/Python/ErrorDisplayed.png)

We can go to the Watch window or Variables window to see what happened. In the sample above we can see that these two variables have an issue. In watch window, we can see values for each variable and one has single quotes and the other one does not. This means one variable is a string and the other is an integer. We cannot add the two because they are different variable types.

![Error](./Images/Python/WatchWindow.png)

We have found our error. We can stop debuggin and fix the issue now.

There are other options in debugging which can be added and explored but for now this will do the trick!!