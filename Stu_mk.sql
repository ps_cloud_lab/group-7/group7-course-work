Create Table Stu_mk
(
Student_ID int,
Marking_period int not null,
English numeric(5,2) not null,
Science numeric(5,2) not null,
Math numeric(5,2) not null,
Social_Studies numeric(5,2) not null,
Physical_Education numeric(5,2) not null,
Year Date not null default current_date,
constraint fk_Stu_Id foreign key (Student_ID) references Student(Student_ID)
);


insert into stu_mk (Student_ID, Marking_period, English, Science, Math,Social_Studies, Physical_Education) values 
(1, 1, 80, 85, 50, 97.9, 48),
(2, 1, 70, 90.5, 90.7, 56.3, 78),
(3, 1, 90, 72, 96, 50, 80),
(4, 1, 100, 88, 50.6, 85, 98);
