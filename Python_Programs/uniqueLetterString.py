class Solution:
    def uniqueLetterString(self, s: str) -> int:
        def uniqueChar(sub: str) -> int:
            for i in sub:
                if sub.count(i) > 1:
                    sub = sub.replace(i, "")
            return len(sub)
        sum=0
        for i in range(len(s)):
            for j in range(i+1, len(s)+1):
                substring = s[i:j]
                sum += uniqueChar(substring)
        return sum

a=Solution()
s=a.uniqueLetterString("ABA")
print(s)
