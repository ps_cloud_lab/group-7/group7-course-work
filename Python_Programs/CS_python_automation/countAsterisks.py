#! /usr/bin/env python
class Solution:
    def countAsterisks(self, s: str) -> int:
        cnt=0
        cntStar=0
        flag = 'N'
        for i in range(len(s)):
            if s[i] =='|':
                cnt+=1
                #print("appending", i,flag)
                if cnt%2==0:
                    flag='Y'
                else:
                    flag='N'
            if flag == 'Y'or (s[i]=='*' and cnt<=0):
                #print("i=",i)
                if s[i]=='*' :
                    cntStar+=1
        return cntStar

solution = Solution()
print(solution.countAsterisks("*||*|||||* | *  |  ***| | * | |***|"))
