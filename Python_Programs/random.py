#random module provides a random() method which generates a float number between 0 and 1
import random
n = random.random()
print(n)
